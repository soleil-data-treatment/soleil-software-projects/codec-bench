#/bin/bash
#
# Return the de\compression performances of a compressor for several power-of-two thread values"

. options.sh
. codec.sh

[ "${USE_HEADER}" = true ] && echo "${CSV_HEADER}"
for THREAD in $(get_thread_list); do
    if [ "${CLEAR_CACHES}" = true ]; then
        clear_caches 3 > /dev/null
    fi
    apply_codec ${COMPRESSOR} ${INFILE} ${LEVEL} ${OUTDIR} ${THREAD}
done
