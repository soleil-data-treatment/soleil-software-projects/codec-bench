#/bin/bash

create_dir() {
    local folder=$1
    if [ -d "${folder}" ]; then
        echo "Directory ${folder} already exists."
    else
        echo "Creating directory: ${folder}"
        mkdir -p ${folder}
    fi
}

dir_exists() {
    local folder=$1
    if [ ! -d "${folder}" ]; then
        echo "Directory ${folder} does not exist"
        exit 1
    fi
}

file_exists() {
    local infile=$1
    if [ ! -f "${infile}" ]; then
        echo "${infile} file does not exist"
        exit 1
    fi
}

get_execution_time() {
    if [  $# -lt 1 ]; then
        echo "Usage: ${FUNCNAME} command -- Return the execution time of a command (in seconds)"
        exit 1
    fi

    LC_NUMERIC=C  # to have a proper decimal-separator
    TIMEFORMAT="%R"  # to have the command time return the result in seconds

    local cmd=$1
    local dummy=$(time (eval ${cmd} 2> /dev/null) 2>&1)
    local duration=$(echo ${dummy} | tr -d  "'")  # remove trialing quotes
    echo "${duration}"
}

get_file_size() {
    if [  $# -lt 1 ]; then
        echo "Usage: ${FUNCNAME} file -- Return the file size (in bytes)"
        exit 1
    fi

    local file=$1
    local size_in_bytes=$(stat --printf="%s" ${file})
    echo ${size_in_bytes}
}

get_nb_procs() {
    local nb_procs=$(cat /proc/cpuinfo | grep processor | wc -l)
    echo ${nb_procs}
}

get_thread_list() {
    method=${1:-1}
    local nb_procs=$(get_nb_procs)
    local n=$(echo ${nb_procs} | awk '{print int(log($1)/log(2))}')  # this the integer part of log2
    case $method in
    0)
        echo $(seq($n))
        ;;
    1)
        # Return the list of all power of 2 with the exponent ranging from 0 to m
        for i in $(seq 0 $n); do
            local pow=$(echo "2^$i" | bc)
            echo "${pow}"
        done
        ;;
    2)
        # Return more values than just the power of 2: 1, 2, 3, [4, 5, 6, 7], [8, 10, 12, 14], [16, 20, 24, 28],  etc.
        for i in $(seq 0 ${n}); do
            local pow=$(echo "2^$i" | bc)
            if [ "$i" -eq "0" ] || [ "$i" -eq "$n" ]; then
                echo "${pow}"
            elif [ "$i" -eq "1" ]; then
                echo "${pow}"
                echo "3"
            else
                for k in $(seq 0 3); do  # cut the interval [2^(n-1), 2^n] in 4 blocks (starting from 4)
                    local a=$(echo "2^($i-2)" | bc)
                    local b=$(echo "$k * $a" | bc)
                    local val=$((pow + b))
                    echo "${val}"
                done
            fi
        done
        ;;
    *)
        echo "Unknown method: ${method}"
        exit 1
        ;;
    esac
}

clear_caches() {
    if [  $# -lt 1 ]; then
        echo "To run this function, PERMISSION must be granted using for instance 'sudo -s'"
        echo "Usage: ${FUNCNAME} num -- Drop memory caches (num=1, 2 or 3)"
        exit 1
    fi

    local num=$1
    if [ "${num}" -lt 1 ] || [ "${num}" -gt 3 ]; then
        >&2 echo "To drop caches, num should be equal to 1, 2 or 3"
        exit 1
    fi
    free -h
    sync
    /usr/bin/echo ${num} > /proc/sys/vm/drop_caches
    free -h
}