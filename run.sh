#/bin/bash
#
# Return the de\compression performances of a given compressor"

. options.sh
. codec.sh

[ "${USE_HEADER}" = true ] && echo "${CSV_HEADER}"
for i in $(seq 1 $REPEAT); do
    if [ "${CLEAR_CACHES}" = true ]; then
        clear_caches 3 > /dev/null
    fi
    apply_codec ${COMPRESSOR} ${INFILE} ${LEVEL} ${OUTDIR} ${THREAD}
done
