#/bin/bash
#set -e -o pipefail

. utils.sh

LEVEL=1
THREAD=
REPEAT=1
OUTDIR=
USE_MKTEMP=false
CLEAR_CACHES=false
USE_HEADER=true
DEBUG=false

COMPRESSOR_LIST=("bzip2" "gzip" "lz4" "pbzip2" "pigz" "pixz" "xz" "zstd")

cleanup() {
    EXIT_CODE=$?
 #   set +e
    if [ "${USE_MKTEMP}" = true ]; then
        [ "${DEBUG}" = true ] && echo "Removing directory: ${OUTDIR}"
        rm -df ${OUTDIR}
    fi
    exit ${EXIT_CODE}
}
trap cleanup EXIT

usage() {
        echo "Usage: $0 INFILE COMPRESSOR"
        echo " The list of available compressors is: ${COMPRESSOR_LIST[@]}"
        echo " [ -l LEVEL ]  -- Compression level [default: ${LEVEL}]"
        echo " [ -p THREAD ] -- Number of threads [default: none]"
        echo " [ -r REPEAT ] - - Run the script multiple times [Ex: 1000] [default: ${REPEAT}]"
        echo " [ -o OUTDIR ]  -- Output directory were the input file will be compressed and decompressed apart [default: mktemp]"
        echo " [ -c ] -- Clear caches --> to use this option, PERMISSION must be granted using for instance 'sudo -s'"
        echo " [ -n ] -- No CSV header"
        echo " [ -d ] -- Debug mode"
}

while getopts "hcdl:no:p:r:" flag; do
    case ${flag} in
        h) 
            usage
            exit 0
            ;;
        c)
            CLEAR_CACHES=true
            ;;
        d)
            DEBUG=true
            ;;
        l)
            LEVEL=${OPTARG}
            ;;
        n)
            USE_HEADER=false
            ;;
        o)
            OUTDIR=${OPTARG}
            ;;
        p)
            THREAD=${OPTARG}
            ;;
        r)
            REPEAT=${OPTARG}
            ;;
        :)
            echo "-${OPTARG} requires an argument."
            exit 1
            ;;
        \?) 
            echo "Invalid option: -${OPTARG}."
            exit 1
            ;;
    esac
done

shift $(( ${OPTIND} - 1 ))

if [  $# -lt 2 ]; then
    usage
    exit 1
fi

INFILE=$1
COMPRESSOR=$2

file_exists "${INFILE}"

if [ -z "${OUTDIR}" ]; then
    OUTDIR=$(mktemp -d)
    USE_MKTEMP=true
else
    dir_exists ${OUTDIR}
fi

INDIR=$(dirname ${INFILE})
if [ "${OUTDIR}" = "${INDIR}" ]; then
    echo "OUTDIR must be different from the directory of INFILE"
    exit 1
fi

if [[ ! " ${COMPRESSOR_LIST[*]} " =~ " ${COMPRESSOR} " ]]; then  # Extra white spaces needed for checking exact match!
    echo "Unknown compressor: ${COMPRESSOR}"
    exit 1
fi

[ "${DEBUG}" = true ] && echo "INFLE: ${INFILE}"
[ "${DEBUG}" = true ] && echo "COMPRESSOR: ${COMPRESSOR}"
[ "${DEBUG}" = true ] && echo "LEVEL: ${LEVEL}"
[ "${DEBUG}" = true ] && echo "OUTDIR: ${OUTDIR}"
[ "${DEBUG}" = true ] && echo "USE_MKTEMP: ${USE_MKTEMP}"
[ "${DEBUG}" = true ] && echo "THREAD: ${THREAD}"
[ "${DEBUG}" = true ] && echo "REPEAT: ${REPEAT}"
[ "${DEBUG}" = true ] && echo "CLEAR_CACHES: ${CLEAR_CACHES}"
[ "${DEBUG}" = true ] && echo "USE_HEADER: ${USE_HEADER}"
[ "${DEBUG}" = true ] && echo "DEBUG: ${DEBUG}"
