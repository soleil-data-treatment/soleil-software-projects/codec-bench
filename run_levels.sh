#/bin/bash
#
# Return the de\compression performances of a compressor for all compression levels"

. codec.sh
. options.sh

read MIN_LEVEL MAX_LEVEL < <(get_compressor_minmax_levels ${COMPRESSOR})

[ "${USE_HEADER}" = true ] && echo "${CSV_HEADER}"
for LEVEL in $(seq ${MIN_LEVEL} ${MAX_LEVEL}); do
    apply_codec ${COMPRESSOR} ${INFILE} ${LEVEL} ${OUTDIR}
done
