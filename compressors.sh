#/bin/bash

get_compressor_version() {
    if [  $# -lt 1 ]; then
        echo "Usage: ${FUNCNAME} compressor -- Return the version of the compressor"
        exit 1
    fi
    local compressor=$1

    local version=
    case $compressor in
        bzip2)
            version=$(bzip2 -V |& head -n 1 | grep -oP '(?<=Version )\K.*(?=,)')
            ;;
        gzip)
            version=$(gzip  -V |& head -n 1 | cut -d' ' -f 2)
            ;;
        lz4 | zstd)
            version=$(${compressor} -V | grep -oP '(?<=v)\K.*(?=,)')
            ;;
        pbzip2)
            version=$(pbzip2 -V |& head -n 1 | cut -d' ' -f 3 | cut -c2-)
            ;;
        pigz)
            version=$(pigz -V | cut -d' ' -f 2)
            ;;
        pixz)
            version=$(pixz -h |& grep -Po "(\d+\.)+\d+")
            ;;
        xz)
            version=$(xz -V |& head -n 1 | cut -d' ' -f 4)
            ;;
        *)
        echo "Unknown compressor: ${compressor}"
        exit 1
        ;;
    esac
    echo ${version}
}

get_compressor_minmax_levels() {
    if [  $# -lt 1 ]; then
        echo "Usage: ${FUNCNAME} compressor -- Return the minimum and maximum compression levels of the compressor"
        exit 1
    fi
    local compressor=$1

    local min_level=
    local max_level=
    case $compressor in
        bzip2 | gzip | pbzip2)
            min_level=1
            max_level=9
            ;;
        lz4)
            min_level=1
            max_level=12
            ;;
        pigz | pixz | xz)
            min_level=0
            max_level=9
            ;;
        zstd)
            min_level=1
            max_level=19
            ;;
        *)
        echo "Unknown compressor: ${compressor}"
        exit 1
        ;;
    esac
    echo "${min_level}" "${max_level}"
}

get_compressor_fullname() {
    if [  $# -lt 2 ]; then
        echo "Usage: ${FUNCNAME} compressor level [thread] -- Return the compressor full name"
        exit 1
    fi
    local compressor=$1
    local level=$2
    local thread=$3

    local version=$(get_compressor_version ${compressor})
    local compressor_name="${compressor} ${version} -${level}"
    if [ -n "${thread}" ]; then
        compressor_name="${compressor_name} -p${thread}"
    fi
    echo ${compressor_name}
}

get_cmd_line_compression() {
    if [  $# -lt 4 ]; then
        echo "Usage: ${FUNCNAME} compressor infile outfile level [thread] -- Return the command line for compressing"
        exit 1
    fi
    local compressor=$1
    local infile=$2
    local outfile=$3
    local level=$4
    local thread=$5

    local cmd=
    case $compressor in
        bzip2 | gzip)
            cmd="${compressor} -f -q -k -${level} ${infile} -c > ${outfile}"
            ;;
        lz4)
            cmd="lz4 -f -q -k -${level} ${infile} ${outfile}"
            ;;
        pbzip2 | pigz)
             cmd="${compressor} -f -q -k -${level} ${infile} -c > ${outfile}"
            [ -n "${thread}" ] && cmd="${cmd} -p${thread}"
            ;;
        pixz)
            cmd="pixz -k -${level} -i ${infile} -o ${outfile}"
            [ -n "${thread}" ] && cmd="${cmd} -p${thread}"
            ;;
        zstd)
            cmd="zstd -f -q -${level} ${infile} -o ${outfile}"
            [ -n "${thread}" ] && cmd="${cmd} -T${thread}"
            ;;
        xz)
            cmd="xz -f -q -k -z -${level} ${infile} -c > ${outfile}"
            [ -n "${thread}" ] && cmd="${cmd} -T${thread}"
            ;;
        *)
        echo "Unknown compressor: ${compressor}"
        exit 1
        ;;
    esac
    echo ${cmd}
}

get_cmd_line_decompression() {
    if [  $# -lt 2 ]; then
        echo "Usage: ${FUNCNAME} compressor infile outfile [thread] -- Return the command line for decompressing"
        exit 1
    fi
    local compressor=$1
    local infile=$2
    local outfile=$3
    local thread=$4

    local cmd=
    case $compressor in
        bzip2 | gzip | xz)
            cmd="${compressor} -f -q -k -d ${infile}"
            ;;
        lz4)
            cmd="lz4 -f -q -k -d ${infile} ${outfile}"  # outfile must be provided otherwise the code will fail for lz4...
            ;;
        pbzip2 | pigz)
            cmd="${compressor} -f -q -k -d ${infile}"
            [ -n "${thread}" ] && cmd="${cmd} -p${thread}"
            ;;
        pixz)
            cmd="pixz -k -d ${infile}"
            [ -n "${thread}" ] && cmd="${cmd} -p${thread}"
            ;;
        zstd)
            cmd="zstd -f -q -d ${infile}"  # No thread option for decompression with zstd
            ;;
        *)
        echo "Unknown compressor: ${compressor}"
        exit 1
        ;;
    esac
    echo ${cmd}
}

get_compressed_file_ext() {
    if [  $# -lt 1 ]; then
        echo "Usage: ${FUNCNAME} compressor -- Return the extension of the compressed file"
        exit 1
    fi
    local compressor=$1

    local ext=
    case $compressor in
        bzip2 | pbzip2)
            ext="bz2";;
        lz4)
            ext="lz4";;
        gzip | pigz)
            ext="gz";;
        pixz | xz)
            ext="xz";;
        zstd)
            ext="zst";;
        *)
        echo "Unknown compressor: ${compressor}"
        exit 1
        ;;
    esac
    echo ${ext}

}