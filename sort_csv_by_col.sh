#/bin/bash

usage() {
    echo "Usage: $0 CSVFILE COLUMN_NO FIELD_SEPARATOR"
    echo "Append a column indicating the row number to CSVFILE and sort by COLUMN_NO+1"
    echo "Original CSVFILE is untouched, output is done on stdout"
}

if [  $# -lt 1 ]; then
    usage
    exit 1
fi

INFILE=$1
COLUMN_NO=${2:-2}
FIELD_SEPARATOR=${3:-','}

KEY=$((${COLUMN_NO}+1))  # Column number has to be updated since we added a column
awk '{printf "%s,%s\n", NR,$0}' ${INFILE} | sort -t ${FIELD_SEPARATOR} -k ${KEY}




