#/bin/bash
#
# Return the de\compression performances of a compressor"

. utils.sh
. compressors.sh
. metrics.sh

CSV_HEADER_FILE="CSV_HEADER.txt"
file_exists ${CSV_HEADER_FILE}
CSV_HEADER=$(<"${CSV_HEADER_FILE}")  # better than doing a 'cat' (avoids both forking a subshell and executing an external command)

if test -z ${DEBUG}; then
    DEBUG=false
fi

apply_codec() {
    if [  $# -lt 4 ]; then
        echo "Usage: ${FUNCNAME} compressor infile level outdir [thread] -- Return the performance metrics of the codec"
        exit 1
    fi
    local compressor=$1
    local infile=$2
    local level=$3
    local outdir=$4
    local thread=$5

    dir_exists "${outdir}"
    file_exists "${infile}"
    local size_raw=$(get_file_size ${infile})

    local ext=$(get_compressed_file_ext ${compressor})
    [ "${DEBUG}" = true ] && echo "Compressed file extension: ${ext}"

    local filename=$(basename ${infile})
    local compressed_outfile="${outdir}/${filename}.${ext}"

    local cmd_line_c=$(get_cmd_line_compression ${compressor} ${infile} ${compressed_outfile} ${level} ${thread})
    [ "${DEBUG}" = true ] && echo "Will run: ${cmd_line_c}"
    local time_c=$(get_execution_time "${cmd_line_c}")  # do not forget quotes!
    [ "${DEBUG}" = true ] && echo "Compression time: ${time_c} second(s)"

    file_exists "${compressed_outfile}"
    local size_c=$(get_file_size ${compressed_outfile})

    local uncompressed_outfile="${compressed_outfile%.*}"
    local cmd_line_d=$(get_cmd_line_decompression ${compressor} ${compressed_outfile} ${uncompressed_outfile} ${thread})
    [ "${DEBUG}" = true ] && echo "Will run: ${cmd_line_d}"
    local time_d=$(get_execution_time "${cmd_line_d}")  # do not forget quotes!
    [ "${DEBUG}" = true ] && echo "Decompression time: ${time_d} second(s)"

    file_exists "${uncompressed_outfile}"

    local compressor_fullname=$(get_compressor_fullname ${compressor} ${level} ${thread})
    print_metrics "${compressor_fullname}" "${size_raw}" "${size_c}" "${time_c}" "${time_d}" "${infile}"

    rm -f "${compressed_outfile}" "${uncompressed_outfile}"
}
