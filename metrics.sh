#/bin/bash

MIB_TO_BYTES=1048576
GIB_TO_BYTES=1073741824

get_duration() {
    if [  $# -lt 2 ]; then
        echo "Usage: $FUNCNAME file_size speed - Return the de\compression time (in s)"
        exit 1
    fi

    local file_size=$1
    local speed=$2
    local duration=$(echo "scale=2; ${file_size} / (${speed} * ${MIB_TO_BYTES})" | bc -l)  # in s
    echo ${duration}
}

get_speed() {
    if [  $# -lt 2 ]; then
        echo "Usage: $FUNCNAME file_size duration - Return the de\compression speed (in MB/s)"
        exit 1
    fi

    local file_size=$1
    local duration=$2
    local speed=$(echo "scale=2; ${file_size} / ${MIB_TO_BYTES} / ${duration}" | bc -l)  # in MB/s
    echo ${speed}
}

get_ratio() {
    if [  $# -lt 2 ]; then
        echo "Usage: $FUNCNAME original_file_size compressed_file_size -- Return the compression ratio"
        exit 1
    fi

    local original_file_size=$1
    local compressed_file_size=$2
    local ratio=$(echo "scale=2; ${original_file_size} / ${compressed_file_size}" | bc -l)
    echo ${ratio}
}

print_metrics() {
    if [  $# -lt 6 ]; then
        echo "Usage: $FUNCNAME compressor_name original_file_size compressed_file_size time_to_compress time_to_decompress infile"
        exit 1
    fi

    local compressor_name=$1
    local size_raw=$2
    local size_c=$3
    local time_c=$4
    local time_d=$5
    local infile=$6

    local speed_c=$(get_speed "${size_raw}" "${time_c}")
    local speed_d=$(get_speed "${size_raw}" "${time_d}")
    local comp_ratio=$(get_ratio "${size_raw}" "${size_c}")
 
    echo "${compressor_name},${speed_c},${speed_d},${size_raw},${size_c},${comp_ratio},${infile}"
}
