# Codec Bench

A script to run performance tests on various compressors.
The script returns a CSV output with the following fields:
- the compressor fullname (name, version, level of compression and optionally the number of threads)
- the compression speed in MB/s
- the decompression speed in MB/s
- the input file size
- the compressed file size
- the compression ratio
- the filename

The supported compressors are: bzip2, gzip, lz4, pbzip2, pigz, pixz, xz, zstd.

# Usage
```
./run.sh -h
```
Basic usage using the input file `infile`(defined as a bash variable) with the compressor `zstd`:
```
./run.sh $infile zstd
```
The results can be saved in a CSV file `outfile.csv`:
```
./run.sh $infile zstd > outfile.csv
```
It is possible to specify the compression level (here 2, default value is 1):
```
./run.sh -l 2 $infile zstd
```
or the number of threads (here 8, default value is none which can have different meanings depending on the compressor):
```
./run.sh -p 8 $infile zstd
```
The performance tests of compression/decompression can be repeated (here 1000 times, default value is 1):
```
./run.sh -r 1000 $infile zstd
```
Tests can be run by cleaning the memory caches at each iteration (asking for permission):
```
sudo -s ./run.sh -c $infile zstd
```
By default, compression and decompression are done in a random directory (so that tests can be done in parallel without interfering with each other).
However, the working directory can be explicitly specified (here `/tmp`):
```
./run.sh -o /tmp $infile zstd
```

To obtain the performance tests over all compression levels:
```
./run_levels.sh $infile zstd
```

To obtain the performance tests using all possible power-of-two threads on your system:
```
./run_threads.sh $infile zstd
```

To quickly verify that the code is working on your computer for all implemented compressors, run this:
```
COMPRESSOR_LIST=("bzip2" "gzip" "lz4" "pbzip2" "pigz" "pixz" "xz" "zstd")
for c in ${COMPRESSOR_LIST[@]}; do ./run.sh $infile $c; done
```
